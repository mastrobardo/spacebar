Tech assessment - Black Jack

the only dependency is mocha/chai for tests.

There are no comments around: good code should be immediatly understandable by itself (there are exceptions where super complicated/not straight-forward hunks of code need to be explained, but this isn't)

please check simpleGame.js to an example of running game
``` node simpleGame.js ```