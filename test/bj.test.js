const BJ = require('../blackjack');
const assert = require('assert');
const chai = require('chai');
const should = chai.should();

describe('deck', () => {
    it('should be exported correctly, and contains 52 cards', () => {
        assert.equal(BJ.deck.length, 52);
    });
});

describe('getANewDeck', () => {
    it('should create a new deck starting from exported deck', () => {
        let copy = BJ.getANewDeck();
        assert.equal(BJ.deck.length, copy.length);
        assert.equal(BJ.deck[0], copy[0])
    })
});

describe('getInitialHand', () => {
    it('should create an hand of specified number of cards', () => {
        let copy = BJ.getANewDeck();
        let hand1 = BJ.getInitialHand(copy);
        let hand2 = BJ.getInitialHand(copy, 6);
        assert.equal(hand1.length, 2);
        assert.equal(hand2.length, 6)
    })
})

describe('pickACardFrom', () => {
    it('should return a card from the deck, and decrease deck length', () => {
        let copy = BJ.getANewDeck();
        let card = BJ.pickACardFrom(copy);
        assert.equal(copy.length, 51);
        card.should.be.an('object');
    })
})

describe('evaluateHand', () => {
    it('should properly count cards', () => {
        let hand = [{value: 1}, {value: 2}];
        let hand2 = [{type: 'A', value: 11}, {type: 'A', value: 11}];
        let result = BJ.evaluateHand(hand);
        let result2 = BJ.evaluateHand(hand2);
        assert.equal(result, 3);
        assert.equal(result2, 12);
    })
})

describe('getDealerHand', () => {
    it('has cards for at least 17 points', () => {
        let copy = BJ.getANewDeck();
        let dealerHand = BJ.getDealerHand(copy);
        let result = BJ.evaluateHand(dealerHand);
    })
})

describe('play', () => {
    it('should work!', () => {
        let bjHand = [{type: 'A', value: 11}, {type: 'K', value: 10}];
        let loseHand = [{type: 'A', value: 11}, {type: '3', value: 3}];
        let bustHand = [{type: 'A', value: 11}, {type: 'A', value: 11}, {type: 'K', value: 10}];
        let notBJHand = [{type: 'A', value: 11}, {type: 'A', value: 11}, {type: '9', value: 9}];
     
     
        let result = BJ.play(bjHand, loseHand);
        assert.equal(result[0], 'DEALER');

        result = BJ.play(bjHand, bjHand);
        assert.equal(result[0], 'NONE');

        result = BJ.play(bustHand, bjHand);
        assert.equal(result[0], 'Player0');

        result = BJ.play(bustHand, notBJHand);
        assert.equal(result[0], 'Player0');

        result = BJ.play(notBJHand, bjHand);
        assert.equal(result[0], 'Player0');

        result = BJ.play(bjHand, notBJHand);
        assert.equal(result[0], 'DEALER');

    })
})