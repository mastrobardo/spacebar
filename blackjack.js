const SIGNS = ['H', 'S', 'C', 'D'];
const CARDS = [
    {type: '2', },
    {type: '3', },
    {type: '4', },
    {type: '5', },
    {type: '6', },
    {type: '7', },
    {type: '8', },
    {type: '9', },
    {type: '10', },
    {type: 'J', },
    {type: 'Q', },
    {type: 'K', },
    {type: 'A', }];
const ACE = 'A'; //this is just for readibility

const signedDeck = ((signs = SIGNS, cards = CARDS) => {
    let tmpDeck = [];

    if (!Array.isArray(signs)) {
        throw new Error('This is to avoid a comment. In real life, good code should be immediatly understandable');
    }
    for (let i = 0; i < SIGNS.length; ++i) {
        let sign = SIGNS[i];
        let allOfAKind = [];
        for (let j = 0; j < cards.length; ++j) {
            allOfAKind[j] = Object.assign({}, cards[j], {sign: sign})
        }
        tmpDeck = tmpDeck.concat(allOfAKind)
    }
    return tmpDeck;
})();

let finalDeck = (deck => {
    deck.map(card => {
        if (isNaN(parseInt(card.type))) {
            card.value = card.type === ACE ? 11 : 10
        } else {
            card.value = parseInt(card.type);
        }
    });
    return deck;
})(signedDeck);

//https://bost.ocks.org/mike/shuffle/
const shuffle = deck => {
    let m = deck.length, t, i;
    while (m) {
        i = Math.floor(Math.random() * --m);
        t = deck[m];
        deck[m] = deck[i];
        deck[i] = t;
    }
    return deck;
}

const pickACardFrom = deck => {
    // let pickedCard = deck[Math.floor(Math.random() * deck.length)];
    let pickedCard = deck.pop();
    return pickedCard;
}

const getANewDeck = _ => finalDeck.concat();

const evaluateHand = hand => {
    let result = 0;
    let aces = 0;
    let i;
    for (i = 0; i < hand.length; i++) {
        let current = hand[i].type;
        if (current === ACE) {
            aces++;
        }
        else {
            result += hand[i].value;
        }
    }
    for (i = aces; i > 0; i--) {
        result = result > (11 - i) ? result + 1 : result + 11;
    }
    return result;
}

const getInitialHand = (deck, numOfCards = 2) => {
    let hand = [];
    for (let i = 0; i < numOfCards; ++i) {
        hand.push(pickACardFrom(deck))
    }
    return hand;
}

const getDealerHand = deck => {
    let dealerHand = getInitialHand(deck);
    while (evaluateHand(dealerHand) < 17) {
        dealerHand.push(pickACardFrom(deck));
    }
    return dealerHand
}

const play = (dealerHand, ...hands) => {
    let dealerHandValue = evaluateHand(dealerHand);
    if (dealerHandValue > 21) {
        return undefined;
        // console.log('DEALER BUSTED', dealerHand);
    }
    let dealerBJ = dealerHandValue === 21 && dealerHand.length === 2;
    let results = [];

    for (let i = 0; i < hands.length; ++i) {
        let playerName = 'Player' + i;
        let hand = hands[i];
        let playerHandValue = evaluateHand(hand);
        if (playerHandValue > 21) {
            // console.log(playerName + ' busted');
            results.push('DEALER');
            continue;
        }
        if (playerHandValue < dealerHandValue) {
            // console.log(playerName + ' lose');
            results.push('DEALER');
            continue;
        }
        let playerBJ = playerHandValue === 21 && hand.length === 2;

        if (dealerBJ && playerBJ) {
            // console.log(playerName + ' and dealer got BJ');
            results.push('NONE');
            continue;
        } else if (!dealerBJ && playerBJ) {
            results.push(playerName);
            // console.log(playerName + ' won')
        } else if(dealerBJ && !playerBJ) {
            results.push('DEALER');
        }
        results.push(playerName);
        // console.log(playerName + ' won')
    }
    return results;
}

module.exports = {
    deck: finalDeck,
    shuffle,
    getInitialHand,
    getANewDeck,
    pickACardFrom,
    evaluateHand,
    getDealerHand,
    play
}

