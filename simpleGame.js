const BJ = require('./blackjack.js');

let copy = BJ.shuffle(BJ.getANewDeck());
let player1 = BJ.getInitialHand(copy);
let player2 = BJ.getInitialHand(copy);
let player3 = BJ.getInitialHand(copy);
let dealerHand = BJ.getDealerHand(copy)
let result = BJ.play(dealerHand, player3, player2);
console.log(result);